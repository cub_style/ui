import "normalize.css";
import "./assets/fonts/UiIcons/style.scss";
import "./assets/styles/index.scss";

import validators from "./plugins/validators";
import sorts from "./plugins/sorts";
import formats from "./plugins/formats";
import parsers from "./plugins/parsers";

import UiBtn from "./components/btn/btn";
import UiBtnGroup from "./components/btn/btn-group";
import UiProgress from "./components/progress";
import UiIcon from "./components/icon";
import UiLoading from "./components/loading";
import UiInputText from "./components/form-elements/input-text";
import UiTooltip from "./components/tooltip";
import UiCard from "./components/card/card";
import UiCardHead from "./components/card/card-head";
import UiCardFooter from "./components/card/card-footer";
import UiTable from "./components/table/table";
import UiTableTheadSort from "./components/table/table-thead-sort";
import UiTableTheadGroup from "./components/table/table-thead-group";
import UiTextarea from "./components/form-elements/textarea";
import UiSelect from "./components/form-elements/select";
import UiDragAndDrop from "./components/drag-and-drop/drag-and-drop";
import UiInputNumber from "./components/form-elements/input-number";
import UiSelectAutocomplete from "./components/form-elements/select-autocomplete";
import UiCheckbox from "./components/form-elements/checkbox";
import UiRadio from "./components/form-elements/radio";

export default {
  install(Vue) {
    Vue.prototype.$uiValidators = validators;
    Vue.prototype.$uiSorts = sorts;
    Vue.prototype.$uiFormats = formats;
    Vue.prototype.$uiParsers = parsers;

    // components
    Vue.component("ui-btn", UiBtn);
    Vue.component("ui-btn-group", UiBtnGroup);
    Vue.component("ui-progress", UiProgress);
    Vue.component("ui-icon", UiIcon);
    Vue.component("ui-loading", UiLoading);
    Vue.component("ui-input-text", UiInputText);
    Vue.component("ui-tooltip", UiTooltip);
    Vue.component("ui-card", UiCard);
    Vue.component("ui-card-head", UiCardHead);
    Vue.component("ui-card-footer", UiCardFooter);
    Vue.component("ui-table", UiTable);
    Vue.component("ui-table-thead-sort", UiTableTheadSort);
    Vue.component("ui-table-thead-group", UiTableTheadGroup);
    Vue.component("ui-textarea", UiTextarea);
    Vue.component("ui-select", UiSelect);
    Vue.component("ui-drag-and-drop", UiDragAndDrop);
    Vue.component("ui-input-number", UiInputNumber);
    Vue.component("ui-select-autocomplete", UiSelectAutocomplete);
    Vue.component("ui-checkbox", UiCheckbox);
    Vue.component("ui-radio", UiRadio);

    // Add $surname instance property directly to Vue components
    Vue.prototype.$surname = "Ui";
  }
};
