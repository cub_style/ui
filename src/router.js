import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      component: () => import("./views/Home.vue")
    },
    {
      path: "/btn",
      component: () => import("./views/Btn.vue")
    },
    {
      path: "/progress",
      component: () => import("./views/Progress.vue")
    },
    {
      path: "/icon",
      component: () => import("./views/Icon.vue")
    }
  ]
});
