import Vue from "vue";

import router from "./router";
import App from "./App.vue";
import Ui from "./index";

Vue.use(Ui);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
