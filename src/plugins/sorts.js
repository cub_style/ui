const sortStringASC = (a, b) => {
  if (a > b) return 1;
  if (a < b) return -1;
  return 0;
};

const sortStringDESC = (a, b) => {
  if (a < b) return 1;
  if (a > b) return -1;
  return 0;
};

export const sortInt = (data, direction) => {
  let localData = Array.from(data);
  return localData.sort((a, b) => {
    if (direction === "DESC") {
      return parseInt(a) - parseInt(b);
    } else if (direction === "ASC") {
      return parseInt(b) - parseInt(a);
    }
  });
};

export const sortIntObject = (data, key, direction) => {
  let localData = Array.from(data);
  return localData.sort((a, b) => {
    if (direction === "DESC") {
      return parseInt(a[key]) - parseInt(b[key]);
    } else if (direction === "ASC") {
      return parseInt(b[key]) - parseInt(a[key]);
    }
  });
};

export const sortString = (data, key, direction) => {
  let localData = Array.from(data);
  return localData.sort((a, b) => {
    if (direction === "DESC") {
      return sortStringDESC(a, b);
    } else if (direction === "ASC") {
      return sortStringASC(a, b);
    }
  });
};

export const sortStringObject = (data, key, direction) => {
  let localData = Array.from(data);
  return localData.sort((a, b) => {
    if (direction === "DESC") {
      return sortStringDESC(a[key], b[key]);
    } else if (direction === "ASC") {
      return sortStringASC(a[key], b[key]);
    }
  });
};

export default {
  sortInt,
  sortString,
  sortIntObject,
  sortStringObject
};
