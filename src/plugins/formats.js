export function numberFormat(
  value,
  decimals = 2,
  dec_point = ".",
  thousands_sep = ""
) {
  if (!value.toString().match(/^(-)?[0-9]+((,|.)?[0-9]+)?$/)) {
    return;
  }

  value = value.toString().replace(/,/g, ".");

  let z = value.substring(0, 1);
  if (z === "-") {
    value = value.replace(/-/, "");
  } else {
    z = "";
  }

  let split = value.split(".");

  split[0] = reverseString(split[0]);
  split[0] = split[0].match(/.{1,3}/g).join(thousands_sep);
  value = reverseString(split[0]);

  if (split[1]) {
    value = value + dec_point.toString() + split[1].substr(0, decimals);
  }

  return z + value;
}

export function reverseString(value) {
  return value
    .split("")
    .reverse()
    .join("");
}

export default {
  numberFormat,
  reverseString
};
