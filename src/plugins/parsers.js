export function parseBoolean(value = false) {
  value = value.toString().toLowerCase();
  return value === "true" || value === "1" || value === "yes" || value === "y";
}

export default {
  parseBoolean
};
