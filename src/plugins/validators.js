export const password = value => {
  return (
    value &&
    /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test(
      value
    )
  );
};

// Валидация заголовков
export const headlineRu = value => {
  return value && /^[а-яА-ЯёЁ.,!?\s0-9_\-()#№"']+$/u.test(value);
};

// Валидация слов
export const alphaRuEn = value => {
  return value && /^[a-zA-zа-яА-ЯёЁ]+$/u.test(value);
};

/**
 * UUID обернут словом SPASIBO
 * UUID: 7577EBE8-AD5C-4272-9B7D-D06674526005
 * S UUID 8 P-A UUID 4 S-I UUID4 B-O UUID4 - UUID 12
 * TEST: S7577EBE8P-AAD5CS-I4272B-O9B7D-D06674526005
 */
export const codeActivation = value => {
  return (
    value &&
    /^S[0-9A-Z]{8}P-A[0-9A-Z]{4}S-I[0-9A-Z]{4}B-O[0-9A-Z]{4}-[0-9A-Z]{12}$/.test(
      value
    )
  );
};

export default {
  password,
  alphaRuEn,
  codeActivation,
  headlineRu
};
